# Primeiro Deployment

Atividade I - Disciplina Sistemas Distribuidos, IFPB. 
Criação da API REST.

## Passo a Passo para a execução da API-REST.

1 - Criação de projeto básico Spring;

2 - No application.properties foi adicionada a propriedade:
- `server.port=${WSPORT:8089}`

Com o `${WSPORT:8089}` a aplicação vai buscar o valor no parâmetro "WSPORT".

3 - Criar o .jar da aplicação com o seguinte comando:

- `mvn clean package`

4 - Criar o Dockerfile da seguinte maneira:

```
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
CMD ["java","-jar","/app.jar", "WSPORT"]

# A linha acima executará o "app.jar" enviando o parâmetro WSPORT que deve ser uma variável de ambiente do container. 
```

5 - Aplicação foi carregada no repositório no git-lab;

6 - Dentro do servidor foi clonada a aplicação;

7 - A imagem da aplicação foi construída e o container criado (a porta escolhida foi a 8080):

- `sudo docker build --tag luan/deploymentsd .`

- `sudo docker run -e WSPORT=8080 -p 8084:8080 --name luan_deployment luan/deploymentsd`
