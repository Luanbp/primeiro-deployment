package com.ifpb.deploymentsd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeploymentsdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeploymentsdApplication.class, args);
	}

}
